const express = require("express");
const port = 4000;

const app = express();

app.use(express.json());

let items = [
		{
			name: "Mjolnir",
			price: 50000,
			isActive: true
		},
		{
			name: "Vibranium Shield",
			price: 70000,
			isActive: true
		}

]

app.get("/", (request, response) => {
	response.send("Hello from my first ExpressJSAPI");
	}
)

app.get("/items", (request, response) => {
	response.send(items);
})

app.post("/items" , (request, response) =>{
	let newItems = {
	name: request.body.name,
	price: request.body.price,
	isActive: request.body.isActive
}

items.push(newItems);
console.log(items);

response.send(items);
})

app.put("/items/:index", (request, response) => {
      console.log(request.body);
        
      console.log(request.params)
      let index = parseInt(request.params.index);
      items[index].price = request.body.price;
      items[index].isActive = request.body.isActive;

response.send(items[index]);

})

app.listen(port, () => console.log(`Server is running at port ${port}`));
